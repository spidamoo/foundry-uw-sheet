class UwItemSheet extends ItemSheet {
  constructor(actor, options) {
    super(actor, options);
  }

  /** @override */
  static get defaultOptions() {
    const options = super.defaultOptions;
    options.template = "modules/uw-sheet/item-sheet.html";  // This will point towards the HTML file you are going to use for the sheet
    options.width = 950; // This configures the default starting width
    options.height = 720; // Starting height
    options.submitOnUnfocus = true;  // Should the form be saved when an input field is unfocused?
    options.submitOnClose = true;  // Should the form be saved when the sheet is closed?
    options.closeOnSubmit = false;  // Should the sheet be closed when it is submitted?
    options.resizable = true;  // Should the sheet be resizable?
    return options;
  }
}

// Items.registerSheet("worldbuilding", UwItemSheet, {makeDefault: true});